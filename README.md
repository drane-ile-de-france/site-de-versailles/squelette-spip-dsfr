# Plugin Squelette Spip DSFR Version 1.11.1

### ATTENTION: Ce design système a uniquement vocation à être utilisé pour des sites officiels de l'état.
### Son but est de rendre la parole de l'état clairement identifiable. [Consulter les CGU](https://www.systeme-de-design.gouv.fr/utilisation-et-organisation/perimetre-d-application)

Elements de références : https://www.systeme-de-design.gouv.fr/


## [Notes de versions](versions.md)

## [Installation](installation.md)

## [Configuration](configuration.md)

## [Utilisation](utilisation.md)

