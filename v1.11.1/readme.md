##Squelette DSFR pour SPIP v 1.7.3

### ATTENTION: Ce design système a uniquement vocation à être utilisé pour des sites officiels de l'état.
### Son but est de rendre la parole de l'état clairement identifiable. [Consulter les CGU](https://www.systeme-de-design.gouv.fr/utilisation-et-organisation/perimetre-d-application)