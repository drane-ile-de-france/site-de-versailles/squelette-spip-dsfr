<?php
/**
 * Définit les autorisations du plugin dfsr
 *
 * @plugin     Ressources
 * @copyright  2022
 * @author     Philippe ROCA
 * @licence    GNU/GPL
 * @package    SPIP\Ressources\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function dsfr_autoriser() {
}

