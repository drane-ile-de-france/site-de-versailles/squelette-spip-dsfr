<?php
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function action_remonter_menu_dist($remonter=null) {

    if (is_null($remonter)) {
        $securiser_action = charger_fonction('securiser_action', 'inc');
        $remonter = $securiser_action();
    }
    $result = sql_select("*", "spip_dsfr", "id_dsfr=$remonter", "", "ordre");
    $row = sql_fetch($result);

    $id_parent=$row["id_parent"];


    $result = sql_select("*", "spip_dsfr", "id_parent=$id_parent", "", "ordre");

		while ($row = sql_fetch($result)) {
			$favori = $row["id_dsfr"];
			$ordre = $row["ordre"];

			if ($favori == $remonter) break;
			else {
				$ordre_prec = $ordre;
				$fav_prec = $favori;
			}
		}

		sql_updateq("spip_dsfr", array("ordre" => $ordre_prec), "id_parent = '$id_parent' AND id_dsfr='$remonter'");
		sql_updateq("spip_dsfr", array("ordre" => $ordre), "id_parent = '$id_parent' AND id_dsfr='$fav_prec'");
    include_spip('inc/invalideur');
  //  suivre_invalideur("favori/$objet/$id_objet");
  //  suivre_invalideur("favori/auteur/$id_auteur");

}

