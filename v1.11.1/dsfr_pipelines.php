<?php
// plugin Squelette DSFR SPIP
// GPL v3

// Modification du flux pour remplacer <quote> par <q> quand
// il n'y a pas de retour � la ligne (sur 3 niveaux, preg sans l'option s !)
function dsfr_pre_propre($flux){

   // if (strpos($flux, "<qu")===false and strpos($flux, "<accordion>")===false) return $flux;

	// gestion des citations
        $flux = str_replace('<quote>','<figure class="fr-quote fr-quote--column fr-mt-4w"><p>', $flux);
        $flux = str_replace('</quote>','</p></blockquote></figure>', $flux);
	      // gestion des accordions
		 $flux = str_replace("<accordion>","<div class=\"fr-accordions-group  fr-my-4w \">", $flux);
		 $flux = str_replace("</accordion>","</div>", $flux);

        if (strpos($flux, '<title>') !== false) {
         $pieces = explode("<title>", $flux);
         $i = 1;
		 $tmp = $pieces[0];
        $max = count($pieces);
             while($i < $max)
             {
                 $tmp .= '<section class="fr-accordion"><h3 class="fr-accordion__title"><button class="fr-accordion__btn" aria-expanded="false" aria-controls="accordion-'.$i.'">'.$pieces[$i];
                 $i++;
             }
            if (strpos($tmp, '<text>') !== false) {
                $pieces = explode("<text>", $tmp);
                $max2 = count($pieces);
                $i = 1;
                $tmp2 = $pieces[0];
                while ($i < $max2) {
                    $tmp2 .= '<div class="fr-collapse" id="accordion-' . $i . '">' . $pieces[$i];
                    $i++;
                }
                $flux=$tmp2;

            }
         }


		 $flux = str_replace("</title>","</button></h3>", $flux);
		$flux = str_replace("</text>","</div></section>", $flux);
		$flux = str_replace('<table class="spip">','<div class="fr-table"><table>', $flux);
		$flux = str_replace('</table>','</table></div>', $flux);
        $flux = accessibiliteLien_pre_liens($flux);
        $flux = linkclass($flux);
    return $flux;
}

function dsfr_insert_head($flux) {
    include_spip('inc/filtres');
    include_spip('inc/config');

    // On fait un md5 de la config pour que le squelette change dès que la config change
    $signature = md5(serialize(lire_config('cookiebar')));

    $lang = (isset($GLOBALS['spip_lang']) ? $GLOBALS['spip_lang'] : 'fr');
    $js_cookiebar = produire_fond_statique('jquery.cookiebar.js', array('lang' => $lang, 'signature' => $signature));

    $flux .= "<script type='text/javascript' src='$js_cookiebar'></script>\n";
    $flux .= "<script type='text/javascript' src='".find_in_path('js/jquery.cookiebar.call.js')."'></script>";

    return $flux;
}


function linkclass($texte) {
    // suppression des class SPIP
    $spipcl = array('/ class="spip_in"/','/ class="spip_out"/');
    $nospipcl = array('','');
    $texte = preg_replace($spipcl, $nospipcl, $texte);

    // inclusion des CLASS perso Part One : début de la CLASS
    $texte = str_replace('#+','" class="',$texte);
    // inclusion des CLASS perso Part Two : fin de la CLASS
    $texte = str_replace('+#','',$texte);

    return $texte;
}

function accessibiliteLien_pre_liens($texte){
    if (!defined('_ACCESSIBILITE_CONSERVER_BULLE')) define('_ACCESSIBILITE_CONSERVER_BULLE', false);
    // Compat SPIP 4.2
    defined('_RACCOURCI_LIEN') || define('_RACCOURCI_LIEN', '/\[([^][]*?([[][^]>-]*[]][^][]*)*)->(>?)([^]]*)\]/msS');
    $regs = $match = array();
    // pour chaque lien
    if (preg_match_all(_RACCOURCI_LIEN, $texte, $regs, PREG_SET_ORDER)) {
        foreach ($regs as $reg) {
            // Attributs du lien (texte, bulle, lang)
            $intitule = traiter_raccourci_lien_atts($reg[1]);
            // si le lien est de type raccourcis "doc40"
            $type = typer_raccourci($reg[4]);
            if (count($type) AND $type[0] == 'document') {
                // Rechercher la taille du Doc dont l'id est dans $type[2]
                $row = sql_fetsel(
                    array('TT1.titre as T1', 'taille', 'TT2.titre as T2'),
                    array('spip_documents AS TT1', 'spip_types_documents AS TT2'),
                    array('id_document='.$type[2], 'TT1.extension=TT2.extension')
                );
                $textelien = ($intitule[0]) ? $intitule[0]:supprimer_numero(typo($row['T1']));
                $langue = ($intitule[2]) ? '{'.$intitule[2].'}':'';
                // Si intitulé du lien, le reprendre,
                // Sinon, si titre pour le doc, le reprendre,
                // Sinon remplacer par "Document"
                $titredoc = ($intitule[0]) ? $intitule[0]:
                    (($row['T1']) ? $row['T1']:_T('info_document'));
                // Quand un title est spécifie il doit etre plus plus long que l'intitule
                // car les lecteurs d'ecran lisent le plus long des deux
                $title = ((($intitule[1]) && _ACCESSIBILITE_CONSERVER_BULLE) ?
                        textebrut(supprimer_numero(typo($intitule[1]))) . ' (' . textebrut(supprimer_numero(typo($titredoc))) . ')':textebrut(supprimer_numero(typo($titredoc)))) // Le texte du lien + Nom du doc
                    . ' &ndash; ' . $row['T2'] // Le type du doc
                    . ' (' . taille_en_octets($row['taille']) . ')' // sa taille
                    . (($intitule[2]) ? ' ('.traduire_nom_langue($intitule[2]).')':''); // La langue presente dans le lien (malheureusement, info non disponible dans la table spip_documents)

                // Rebatir le raccourcis typo du lien avec les informations modifiees
                $lien = '['. $textelien . '|'. $title .$langue .'->'. $reg[4] .']';
                $texte = str_replace($reg[0], $lien, $texte);
            }
        }
    }
    return $texte;
}
