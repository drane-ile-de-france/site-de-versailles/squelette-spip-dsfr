<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

function formulaires_dsfr_menu_modifier_charger_dist($id_menu, $id_dsfr) {
	if (is_null($id_dsfr)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_dsfr = $securiser_action();
	}
	$val=array();

	$result = sql_select("*", "spip_dsfr", "id_dsfr=$id_dsfr");
	$row = sql_fetch($result);

	$val['id_menu'] = $id_menu;
	$val['id_dsfr'] = $id_dsfr;
	$val['id_parent'] = $row['id_parent'];
	$val['id_parent'] = $row['id_parent'];
	$val['titre'] = $row['titre'];
	$val['texte'] = $row['texte'];
	$val['objet'] = $row['objet'];
	$val['id_objet'] = $row['id_objet'];
	$val['url'] = $row['url'];
	$val['ordre'] = $row['ordre'];

	return $val;
}

function formulaires_dsfr_menu_modifier_traiter_dist() {
	$id_menu=_request('id_menu');
	$id_dsfr=_request('id_dsfr');
	$titre=_request('titre');
	$id_parent=_request('id_parent');
	$old_parent=_request('old_parent');
	$objet=_request('objet');
	$id_objet=_request('id_objet');
	$url=_request('url');
	$texte=_request('texte');
	$ordre =_request('next');
	$old_ordre =_request('old_ordre');
	if ($id_parent == $old_parent){
		$ordre = $old_ordre;
	}
	sql_updateq("spip_dsfr", array(
		'titre' => $titre,
		'id_parent' => $id_parent,
		'id_objet' => $id_objet,
		'url' => $url,
		'ordre' => $ordre	,
		'texte' => $texte
	), "id_dsfr='$id_dsfr'");


		$res['redirect'] = '?exec=menu_dsfr&id_menu=' . $id_menu;
		return $res;



}
