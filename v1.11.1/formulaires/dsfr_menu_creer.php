<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

function formulaires_dsfr_menu_creer_charger_dist() {
	$val['titre'] = '';
	return $val;
}

function formulaires_dsfr_menu_creer_traiter_dist() {
	$titre=_request('titre');
	if ($titre != ""){

//		$id = sql_insertq("spip_dsfr", "(titre , id_parent)", "('$titre', 0)");
		$id = sql_insertq("spip_dsfr", array('titre' => $titre, 'id_parent' => 0));
		$res['redirect'] = '?exec=menu_dsfr&id_dsfr=' . $id;
		return $res;

	}else{
		return ['message_erreur' => 'Titre vide', 'editable' => true];

	}

}
