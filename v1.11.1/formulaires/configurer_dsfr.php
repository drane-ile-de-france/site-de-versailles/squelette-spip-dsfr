<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

function formulaires_configurer_dsfr_charger_dist() {
    include_spip('inc/filtres');
    include_spip('inc/config');
    $config = lire_config('dsfr', []);

return $config;
}

function formulaires_configurer_dsfr_traiter_dist() {
    include_spip('inc/filtres');
    include_spip('inc/config');
include_spip('inc/meta');

    $config['institution']=_request('institution');
    $config['choixMenu']=_request('choixMenu');
    $config['menuType']=_request('menuType');
    $config['reseauxSociaux']=_request('reseauxSociaux');
    $config['Facebook']=_request('Facebook');
    $config['Twitter']=_request('Twitter');
    $config['Instagram']=_request('Instagram');
    $config['Linkedin']=_request('Linkedin');
    $config['Youtube']=_request('Youtube');
    $config['Mastodon']=_request('Mastodon');
    $config['idMentions']=_request('idMentions');
    $config['idDonnees']=_request('idDonnees');
    $config['idCookies']=_request('idCookies');
    $config['rgaa']=_request('rgaa');
    $config['mentionInstitution']=_request('mentionInstitution');
    $config['mentionPublication']=_request('mentionPublication');
    $config['mentionEdition']=_request('mentionEdition');
    $config['hebergeurNom']=_request('hebergeurNom');
    $config['hebergeurAdresse']=_request('hebergeurAdresse');
    $config['webmestreNom']=_request('webmestreNom');
    $config['webmestreId']=_request('webmestreId');
    $config['contactId']=_request('contactId');
    $config['dpdMail']=_request('dpdMail');
    $config['dpdEntite']=_request('dpdEntite');
    $config['dpdAdresse']=_request('dpdAdresse');
	$config['videoId']=_request('videoId');
	$config['videoH1']=_request('videoH1');
	$config['videoH2']=_request('videoH2');
	$config['afficheAgenda']=_request('afficheAgenda');
	$config['connexionHaut']=_request('connexionHaut');
	$config['connexionBas']=_request('connexionBas');
	$config['logoSite']=_request('logoSite');
	$config['masquerAuteur']=_request('masquerAuteur');
	$config['siteDescriptif']=_request('siteDescriptif');
	$config['siteActualite']=_request('siteActualite');
	$config['paginationNb']=_request('paginationNb');
	$config['paginationDefilement']=_request('paginationDefilement');
	$config['partenaires']=_request('partenaires');
	$config['bgSocial']=_request('bgSocial');
	$config['bgCarteArticle']=_request('bgCarteArticle');
	$config['bgTuileArticle']=_request('bgTuileArticle');
	$config['bgBandeauArticle']=_request('bgBandeauArticle');
	$config['bgCallOutArticle']=_request('bgCallOutArticle');
	$config['bgCallOutMot']=_request('bgCallOutMot');
	$config['bgCarteMot']=_request('bgCarteMot');
	$config['bgTuileMot']=_request('bgTuileMot');
	$config['bgCarteRubrique']=_request('bgCarteRubrique');
	$config['bgTuileRubrique']=_request('bgTuileRubrique');
	$config['bgTuileSite']=_request('bgTuileSite');
	$config['bgTuileDocument']=_request('bgTuileDocument');
	$config['bgBandeauSommaire']=_request('bgBandeauSommaire');
	$config['bandeauClic']=_request('bandeauClic');
	$config['posLienRapide']=_request('posLienRapide');
	$config['filtreRecherche']=_request('filtreRecherche');
	$config['partage']=_request('partage');


        ecrire_meta('dsfr', serialize($config));


return ['message_ok' => _T('config_info_enregistree'), 'editable' => true];
}
