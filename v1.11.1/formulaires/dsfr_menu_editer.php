<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

function formulaires_dsfr_menu_editer_charger_dist($id_menu, $id_modif) {
	$val['id_menu'] = $id_menu;
	$val['id_modif'] = $id_modif;
	return $val;
}

function formulaires_dsfr_menu_editer_traiter_dist() {
	$id_menu=_request('id_menu');
	$titre=_request('titre');
	$id_parent=_request('id_parent');
	$objet=_request('objet');
	$id_objet=_request('id_objet');
	$url=_request('url');
	$texte=_request('texte');
	$ordre =_request('next');

		$id = sql_insertq("spip_dsfr", array(
			'titre' => $titre,
			'id_parent' => $id_parent,
			'objet' => $objet,
			'id_objet' => $id_objet,
			'url' => $url,
			'ordre' => $ordre	,
			'texte' => $texte
		));
		$res['redirect'] = '?exec=menu_dsfr&id_menu=' . $id_menu;
		return $res;



}
