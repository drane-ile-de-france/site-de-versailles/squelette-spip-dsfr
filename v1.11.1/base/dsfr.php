<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


function dsfr_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['dsfr'] = 'dsfr';


	return $interfaces;
}

function dsfr_declarer_tables_objets_sql($tables) {

	$tables['spip_dsfr'] = array(
		'field' => array(
			'id_dsfr'       => 'bigint(21) NOT NULL AUTO_INCREMENT',
			'id_parent'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'titre'              => 'varchar(255) DEFAULT "" NOT NULL',
			'texte'              => 'longtext DEFAULT "" NOT NULL',
			'objet'              => 'varchar(255) DEFAULT "" NOT NULL',
			'id_objet'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'url'              => 'varchar(255) DEFAULT "" NOT NULL',
			'ordre'              => 'bigint(21) DEFAULT "0" NOT NULL',

		),
		'key' => array(
			'PRIMARY KEY'        => 'id_dsfr',
			'KEY id_parent'   => 'id_parent',
		)
	);

	return $tables;
}

