# Plugin Squelette Spip DSFR Installation


Ce plugin nécessite les plugins
- Saisie pour formulaires
- Selection d'articles
- Agenda
- Critère mots

Besoin de création de mots clés.

Pour les rubriques, activer le descriptif.
Pour les articles, activer surtitre, sous-titre, chapo et descriptif.

## Création des mots clés

### "DSFR_Menu"
Permet d'afficher une rubrique dans la barre de menu (pour le menu automatique)

### "DSFR_Bandeau"
Permet d'afficher un article et une image en bandeau avant le carousel

### "DSFR_Carousel"
Permet d'afficher un à trois articles en carousel en haut de la page sommaire

### "DSFR_CallOut"
Permet d'afficher un article en avant sour la carousel de la page sommaire

### "DSFR_LienRapide"
Permet d'afficher les articles en petites tuiles verticales sur la page d'accueil.
Le descriptif fait office de titre sur la page d'accueil

### "DSFR_A_la_une"
Permet d'afficher les articles en grosses tuiles horizontales sur la page d'accueil

### "DSFR_Partenaire_Gauche"
Permet d'afficher le logo d'un lien à gauche dans le footer

### "DSFR_Partenaire_Droite"
Permet d'afficher des logos de liens à droite dans le footer

### "DSFR_Site_institutionnel"
Permet d'afficher le lien vers le site dans le footer

### "DSFR_Rubrique_Filtre"
Permet d'activer les filtres d'articles sur une rubrique

