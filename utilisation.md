# Plugin Squelette Spip DSFR Utilisation

## Rédaction des textes

### Images et vidéos accessibles

Pour les vidéos ou images porteuses de sens, il faut renseigner 

- le titre du document
- le descriptif du document qui servirra de transcription
- les crédits
- le texte alternatif.

Pour afficher le document en mode accessible, il faut ajouter l'attribut **access** à la balise.

```<docXX|access>```

La taille du document doit être placée à la fin.

```<docXX|access|largeur>```

### Liens accessibles
```[ Texte cliquable | Texte info bulle -> http://..... ]``` 

Permet d'afficher une info bulle sur les liens

### Lien en bouton

Pour transformer un lien en bouton, il faut ajouter la classe comme ceci

```[ mon texte|texte info bulle-> url_page#+classe+#]```

Vous pouvez utiliser les classes suivantes :

- **fr-btn** pour un bouton bleu
- **fr-btn fr-btn--secondary** pour un bouton blanc à bord bleu

Exemples :

```[ac-versailles|Académie de Versailles->https://wwwac-versailles.fr#+fr-btn+#]```

```[ac-versailles|Académie de Versailles->https://wwwac-versailles.fr#+fr-btn fr-btn--secondary+#]```

### Les citations
```<quote> ... </quote>```

### Les blocs de code
```<code> ... </code>```

### Les blocs dépliables
  ```
  <accordion>
    <title>
      Le titre du premier bloc
    </title>
    <text>
      Le texte du premier bloc
    </text>
    <title>
      Le titre du second bloc
    </title>
    <text>
      Le texte du second bloc
    </text>
    ...
  </accordion>
  ```
Il peut y a avoir plusieurs groupes de blocs dans une même page


Pour les **images** affichées dans les articles, il faut bien saisir la transcription dans la partie *description* du document. On a alors un bouton *voir la transcription* qui s'affiche à bas à droite de l'image.

