### Ce plugin pour SPIP est sous licence  [GNU AGPL-3](https://www.gnu.org/licenses/agpl-3.0.fr.html)

### ATTENTION: Le design système a uniquement vocation à être utilisé pour des sites officiels de l'état.
### Son but est de rendre la parole de l'état clairement identifiable. [Consulter les CGU](https://www.systeme-de-design.gouv.fr/utilisation-et-organisation/perimetre-d-application)

Elements de références : https://www.systeme-de-design.gouv.fr/
