# Plugin Squelette Spip DSFR notes de versions

**Version 1.11.0**
- Fix prolème pipeline

**Version 1.11.0**
- Passage au DSFR version 1.11.0
- Bouton Back To Top
- Changement du logo Twitter en X
- Possibilité de masquer les liens de connexion SPIP dans le header et le footer
- Possibilité d'affichage du logo du site dans le header
- Possibilité de masquer les auteurs des articles
- Affichage des rubriques dans le menu {par num titre}
- Liens de partage des pages sur les réseaux sociaux
- Pagination à 8 articles dans les rubriques personnalisable
- Lien vers la rubrique principale dans le simple menu
- Affiche de la page site avec flux RSS pour les sites syndiqués
- Possibilité d'affichage des articles dans une rubrique en défilement infini
- Affichage et personnalisation des partenaires dans le footer avec les mots clés DSFR_Partenaire_Gauche et DSFR_Partenaire_Droite
- Utilisation du mot clé DSFR_Rubrique_Filtre pour activer le filtre sur les rubriques
- Personnalisation des couleurs
- Utilisation sur mot clé DSFR_Bandeau pour faire un gros bandeau avec un article et une image
- Tri des articles à la une par date inverse et non par num titre
- Utiliser le descriptif du mot clé DSFR_A_la_une pour titre la zone sur la page d'accueil
- Choix de la position des liens rapides (en haut ou en bas)
- Possibilité de filtrer les recherches par mots clés

**Version 1.7.3** 
- Correctifs 1.7.2 pour sites établissements

**Version 1.7.2** 
- Passage au DSFR 1.7.2

**Version 1.4.0** 
- Pemière version sortie
