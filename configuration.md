# Plugin Squelette Spip DSFR Configuration

Aller sur l'adresse https://monsite.ac-academie.fr/ecrire/?exec=configurer_dsfr

### Onglet Accueil

#### Entète

**Institution :** On définit le nom de l'institution qui sert à générer le logo en haut à gauche dans le bandeau. On peut y saisir la balise ```<br>``` pour forcer le retour à la ligne.

**Afficher le logo du site SPIP :** On choisit si le logo du site SPIP est affiché dans le bandeau à droite du logo de l'institution.


**Choix du menu :** On choisit quel contenu de menu sera utilisé. Soit un menu basé sur les rubriques qui ont le mot clé **menu**, soit on appelle un menu que l'on a créé personnellement comme expliqué plus loin.

**Format du menu :** On choisit le look du menu, soit simple déroulant, soit big menu à 4 colonnes.

**Afficher les liens de connexion SPIP :** On choisit de masquer le bouton "connexion", "espace privé"... du header lorsque l'on est connecté.

#### Vidéo d'accueil

On peut afficher une vidéo en plein écran lors de la première visite sur le site.

**ID du document mp4 :** La vidéo est désposée en pièce jointe d'un article. C'est l'id de ce document qu'il faut renseigner. Si le champ reste vide, l'accès au site est direct.

**Titre h1**  et **Titre h2 :** Lorsque l'on affiche la vidéo, au dessus du bouton *accéder au site** on peut mettre 2 lignes de texte qui seront mis en balises ```<h1>``` puis ```<h2>```. Chacune est facultative.



#### Pied de lpage

**Afficher les liens de connexion SPIP :** On choisit de masquer le bouton "connexion", "espace privé"... du footer lorsque l'on est connecté.

**Titre de la zone Partenaire :** Les mots clés DSFR_Partenaire_XXX affichent les logos des sites dans le footer, on peut donner un titre à cette zone. Le titre peut restere vide.



#### Réseaux sociaux

Pour chaque réseau social, si l'url est saisi, l'icône lien apparaîtra sur la page d'accueil.





#### Page d'accueil

**Afficher les événements :**  On peut choisr d'afficher ou pas les évènements sur la page d'accueil à coté des articles d'actualité. Si on les affiche, la pagination et la taille des vignettes s'adapte.

**Rendre l'article d'accueil cliquable :** L'article du bandeau (mot clé DSFR_Bandeau) peut être ouvert (cliquable) ou juste affiché pour information.

**Position des liens rapides :** les articles utilisant le mot clé DSFR_LienRapide peuvent être positionné en haut de page sous le bandeau ou en bas de page juste avant le footer



#### Pages Rubriques

**Affichage des articles :** Par défaut, on utilise la pagination de SPIP pours lister les articles (page1, 2, 3, précente, suivante). On peut choisir d'avoir un bouton "Voir plus" qui affiche des lots supplémentaires d'article sans masquer ceux déjà visibles.

**Nombre d'articles par page ou défilement :** Par défaut, les articles d'affichent pas pages ou lots de 8. On peut changer. Il est conseillé d'utiliser un multiple de 4.




#### Pages Articles

**Masquer les auteurs :** Par défaut, le chapo d'un article affiche ses auteurs, on peut choisir de les masquer.

**Nombre d'articles par page ou défilement :** Par défaut, les articles d'affichent pas pages ou lots de 8. On peut changer. Il est conseillé d'utiliser un multiple de 4.


#### Les sites référencés d'une rubrique

**Afficher le descriptif dans les tuiles des rubriques :** Dans les tuiles qui listent les sites référencés, on peut choisir d'yy afficher le descriptif du site. Attention à ce qu'il ne soit pas trop long.

**Ouvrir l'actualité du site :** Si un site référencé a sa syndication activée (on récupère l'actualité du site, le flux RSS), on peut choisir d'afficher sa liste d'articles au lieu de l'ouvrir directement.





### Onglet Mentions légales



#### Informations légales

**Mentions légales :** Les mentions légalements sont préconstruites avec les informations sur second onglet de configuration. Il est possible de venir les compléter avec le contenu du texte d'un article dont on saisit l'ID dans ce champ.

**Données personnelles :** On crée un article qui indique quelles et comment les données personnelles sont utilisées. C'est le contenu de cet article qui sera affiché sur une page en complément des coordonnées du DPDP.

**Gestion des cookies :** On crée un article qui servirra à la cookie bar et au modal. Dans la cookie bar, on affiche le chapo de cet article. Lors que l'on clique sur le bouton *gestion des cookies** en pied de page, on ouvre un modal qui affiche alors le texte dee cet article

**Accessibilité RGAA :** Le code est pas trop mal niveau RGAA... Le gros du travail est de l'ordre rédactionnel. Il faut bien penser à renseigner toutes les informations sur les documents et images envoyés.


La base des mentions légales est un texte à trous. Il suffit de compléter les champs pour les boucher.


